processor: pipelinecycle.o cpu.o memory.o syscall.o 
	gcc pipelinecycle.o cpu.o memory.o syscall.o -o processor

pipelinecycle.o: pipelinecycle.c
	gcc -c main.c

cpu.o: cpu.c
	gcc -c cpu.c

memory.o: memory.c
	gcc -c memory.c

syscall.o: syscall.c
	gcc -c syscall.c

clean:
	rm *.o processor