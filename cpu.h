#pragma once
/**
* Ruel Gordon
* Matthew King
* LeAnn Lewis
* Gedare Bloom
* cpu.h
*
* Definitions for the processor.
*/

#include <stdint.h> 
#include <inttypes.h>
#include <stdio.h>

struct cpu_context {
	uint32_t PC;
	uint32_t GPR[32];
};

extern struct cpu_context cpu_ctx;

struct IF_ID_buffer {
	uint32_t instruction;
	uint32_t next_pc;
};


//start of ID component

typedef struct
{
	uint32_t ALUSrc;
	uint32_t RegDst;
}ExecuteSignals;

typedef struct
{
	//uint32_t Branch;
	uint32_t MemRead;
	uint32_t MemWrite;
}MemorySignals;

typedef struct
{
	uint32_t MemtoReg;
	uint32_t RegWrite;
}WriteBackSignals;

typedef struct
{
	uint32_t Branch;
	uint32_t Jump;
}DecodeSignals;
typedef struct
{
	DecodeSignals dePhase;
	ExecuteSignals exPhase;
	MemorySignals memPhase;
	WriteBackSignals writeBackPhase;
}CUSignalType;

typedef struct
{
	CUSignalType signals;
	uint32_t read1Cont;
	uint32_t read2Cont;
	uint32_t Cont1Address;
	uint32_t Cont2Address;
	uint32_t opCode;
	uint32_t funct;
	uint32_t aluOP;
	uint32_t extendedImmediate;
	uint32_t jumpInstruction;
	uint32_t writeAddressRType;
	uint32_t writeAddressIType;
}ID_EX_buffer;


typedef struct
{
	uint32_t opCode;
	uint32_t read1Address;
	uint32_t read2Address;
	uint32_t writeAddressRType;
	uint32_t writeAddressIType;
	uint32_t shamt;
	uint32_t funct;
	uint16_t immediate;
	uint32_t immediate_26;
}InstructionComponents;

//end of ID component

struct EX_MEM_buffer {
	CUSignalType signals;
	uint32_t address;
	uint32_t writeData;
	uint32_t writeAddressRType;
	uint32_t writeAddressIType;
	uint32_t destinationReg;
};

struct MEM_WB_buffer {
	CUSignalType signals;
	uint32_t memdata;
	uint32_t ALUresult;
	uint32_t writeAddressRType;
	uint32_t writeAddressIType;
	uint32_t destinationReg;
};

typedef struct {
	int loadIsTrue;
	uint32_t address;
}hazard_data;

int fetch(struct IF_ID_buffer *out);
int decode(struct IF_ID_buffer *in, ID_EX_buffer *out, struct EX_MEM_buffer *ex_mem, struct MEM_WB_buffer *mem_wb, hazard_data* hazardDetection);
int execute(ID_EX_buffer *in, struct EX_MEM_buffer *out, struct MEM_WB_buffer *MEM);
int memory(struct EX_MEM_buffer *in, struct MEM_WB_buffer *out);
int writeback(struct MEM_WB_buffer *in);
InstructionComponents separateComponents(uint32_t instruction);
uint32_t generateAluOp(uint32_t opCode, uint32_t funct);
CUSignalType generateControlSignals(uint32_t opCode, uint32_t instruction);
ID_EX_buffer updateReadValues(InstructionComponents passedComponents, ID_EX_buffer outBuffer, uint32_t GPR[]);
uint32_t signExtendValue(uint16_t passedValue);
uint32_t rw_memory(uint32_t ALUresult, uint32_t data2, uint32_t MemWrite, uint32_t MemRead, uint32_t *memdata, uint32_t Mem[]);
void write_register(uint32_t r2, uint32_t r3, uint32_t memdata, uint32_t ALUresult, uint32_t RegWrite, uint32_t RegDst, uint32_t MemtoReg, uint32_t *Reg);
uint32_t ALU(ID_EX_buffer *in, uint32_t Reg2);
uint32_t Adder(ID_EX_buffer *in, uint32_t Reg2);
uint32_t Multiplexor(ID_EX_buffer *in);
uint32_t PC_Counter(uint32_t PC);
uint32_t changeToDecimal(char passedVal[], int valBase);
int modifyCharacter(char passedCharacter);
int convertPCToInstructionIndex(int givenPCValue);
int convertMemoryAddressToMemoryIndex(uint32_t passedAddress);
int syscall(ID_EX_buffer *ID_EX, struct cpu_context CPU);
int jump(struct IF_ID_buffer *in, InstructionComponents Instructions, ID_EX_buffer* out);
int jumpAndLink(struct IF_ID_buffer *in, InstructionComponents Instructions, ID_EX_buffer* out);
int jumpRegister(struct IF_ID_buffer *in, ID_EX_buffer* out);
int checkToForward(uint32_t incomingAddress, uint32_t exMemAddress, uint32_t wbAddress);
uint32_t forwarder(struct EX_MEM_buffer *EX_MEM, struct MEM_WB_buffer *MEM_WB, uint32_t incomingAddress, uint32_t exMemAddress, uint32_t wbAddress);
int BranchEqual(struct IF_ID_buffer* in, InstructionComponents Instruction, ID_EX_buffer* out);
int newBranchEqual(struct IF_ID_buffer* in, uint32_t value1, uint32_t value2, uint32_t immediate, ID_EX_buffer* out);
int BranchNotEqual(struct IF_ID_buffer* in, InstructionComponents Instruction, ID_EX_buffer* out);
int newBranchNotEqual(struct IF_ID_buffer* in, uint32_t value1, uint32_t value2, uint32_t immediate, ID_EX_buffer* out);
int hazardDetection(struct IF_ID_buffer *in, InstructionComponents Instructions, ID_EX_buffer* out, hazard_data* hazardData);
