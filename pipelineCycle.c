/**
* Gedare Bloom
* single-cycle.c
*
* Drives the simulation of a single-cycle processor
*/

#include "cpu.h"
#include "memory.h"
#include "syscall.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <math.h>

#define DEBUG

enum ASCIICONSTANTSI { AUVALUE = 65, BUVALUE, CUVALUE, DUVALUE, EUVALUE, FUVALUE, GUVALUE, HUVALUE, IUVALUE, JUVALUE, KUVALUE, LUVALUE, MUVALUE, NUVALUE, OUVALUE, PUVALUE, QUVALUE, RUVALUE, SUVALUE, TUVALUE, UUVALUE, VUVALUE, WUVALUE, XUVALUE, YUVALUE, ZUVALUE };

enum ASCIICONSTANTSII { ALVALUE = 97, BLVALUE, CLVALUE, DLVALUE, ELVALUE, FLVALUE, GLVALUE, HLVALUE, ILVALUE, JLVALUE, KLVALUE, LLVALUE, MLVALUE, NLVALUE, OLVALUE, PLVALUE, QLVALUE, RLVALUE, SLVALUE, TLVALUE, ULVALUE, VLVALUE, WLVALUE, XLVALUE, YLVALUE, ZLVALUE };

enum ASCIICONSTANTSIII { TABVALUE = 9, APOSTROPHEVALUE = 39, OPENBRACKETVALUE, CLOSEDBRACKETVALUE, ASTERISKVALUE, PLUSVALUE, COMMAVALUE, DASHVALUE, PERIODVALUE, BACKSLASHVALUE };

enum ASCIICONSTANTSIV { COLONVALUE = 58, SEMICOLONVALUE, GREATERTHANVALUE, EQUALVALUE, LESSTHANVALUE, QUESTIONMARKVALUE, ATVALUE };

enum ASCIICONSTANTSV { ACVALUE = 1, BCVALUE, CCVALUE, DCVALUE, ECVALUE, FCVALUE, GCVALUE, HCVALUE, ICVALUE, JCVALUE, KCVALUE, LCVALUE, MCVALUE, NCVALUE, OCVALUE, PCVALUE, QCVALUE, RCVALUE, SCVALUE, TCVALUE, UCVALUE, VCVALUE, WCVALUE, XCVALUE, YCVALUE, ZCVALUE };

enum ASCIINAVIGATIONI { BACKSPACE = 8, ENTER = 13, LCHARLIMIT = 32, UCHARLIMIT = 128 };

enum ASCIINAVIGATIONII { ARROW = 224, LEFT = 75, RIGHT = 77, UP = 72, DOWN = 80, SLEFT = 115, SRIGHT = 116, SUP = 141, SDOWN = 145 };



int main(int argc, char *argv[])
{
	FILE *loadManager;
	struct IF_ID_buffer if_id;
	ID_EX_buffer id_ex;
	struct EX_MEM_buffer ex_mem;
	struct MEM_WB_buffer mem_wb;
	struct MEM_WB_buffer mem_wbOLD;
	hazard_data hazDect;
	hazDect.address = 0; hazDect.loadIsTrue = 0;
	int i, completionChecker;
	char lowerHalfWord[5];
	char fullWord[9];
	float cycles;
	float cpi;

	squashed = 0;
	cycles = 0;
	completionChecker = 0;


	/* Initialize registers and memory to 0 */
	cpu_ctx.PC = 536870912; // 0x20000000
	for (i = 0; i < 32; i++) {
		cpu_ctx.GPR[i] = 0;
	}
	cpu_ctx.GPR[29] = 268451840; //setting $sp to 0x10004000

	for (i = 0; i < 1024; i++) {
		instruction_memory[i] = 0;
		stack_memory[i] = 0;
	}
	for (i = 0; i < 2048; i++)
	{
		data_memory[i] = 0;
	}

	/* Read memory from the input file */
	loadManager = fopen("program.sim", "r"); //Assumed your computer will read .sim files without error

	for (i = 0; i < 1024; i++) {
		cycles++;
		//fread(&instruction_memory[i], sizeof(uint32_t), 1, f);
		fscanf(loadManager, "%s", fullWord);
		fscanf(loadManager, "%s", lowerHalfWord);
		strcat(fullWord, lowerHalfWord);
		instruction_memory[i] = changeToDecimal(fullWord, 16);
#if defined(DEBUG)
		printf("%i\n", instruction_memory[i]);
#endif
	}
		for (i = 0; i < 1024; i++) {
			cycles++;
			//fread(&instruction_memory[i], sizeof(uint32_t), 1, f);
			fscanf(loadManager, "%s", fullWord);
			fscanf(loadManager, "%s", lowerHalfWord);
			strcat(fullWord, lowerHalfWord);
			data_memory[i] = changeToDecimal(fullWord, 16);
#if defined(DEBUG)
			printf("%i\n", data_memory[i]);
#endif

		}
		fclose(loadManager);

		cpu_ctx.GPR[29] = 268443648; //set sp to base of stack

		initializeSignals(&if_id, &id_ex, &ex_mem, &mem_wb);
		if_id.instruction = 0; //you might want to delete this later

		while (1) {
#if defined(DEBUG)
			printf("FETCH from PC=%x\n", cpu_ctx.PC);
#endif

			writeback(&mem_wb);
			memory(&ex_mem, &mem_wb);
			execute(&id_ex, &ex_mem, &mem_wbOLD);
			decode(&if_id, &id_ex, &ex_mem, &mem_wbOLD, &hazDect);
			fetch(&if_id);
			if (if_id.instruction == 0)
			{
				completionChecker++;
				if (completionChecker == 5)
				{
					break;
				}
			}
			else
			{
				completionChecker = 0;
			}
			//if (cpu_ctx.PC == 0) break;

			mem_wbOLD = mem_wb;

			cpu_ctx.PC = if_id.next_pc;
		}

		cpi = ((cycles - squashed) / cycles);

		printf("CYCLES: %f\n", cycles);
		printf("SQUASHED: %f\n", squashed);
		printf("CPI: %f\n", cpi);

		system("pause");
		return 0;

	}

	uint32_t signExtendValue(uint16_t passedValue)
	{
		uint32_t extendedValue;
		extendedValue = passedValue;
		if ((passedValue - 32768) >= 0) //if lestmost bit is 1
		{
			extendedValue += 4294901760; //make upper half all 1s to make value still negative
		}
		return extendedValue;
	}

	/***************************************************************************************************************/

	InstructionComponents separateComponents(uint32_t instruction)
	{
		InstructionComponents currentInstruction;

		currentInstruction.opCode = (instruction & 4227858432) >> 26; //11111100000000000000000000000000 & Isolate then shift the opCode
		currentInstruction.read1Address = (instruction & 65011712) >> 21; //00000011111000000000000000000000 & Isolate then shift the readAddress1
		currentInstruction.read2Address = (instruction & 2031616) >> 16; //00000000000111110000000000000000 & Isolate then shift the readAddress2
		currentInstruction.writeAddressRType = (instruction & 63488) >> 11; //00000000000000001111100000000000 & Isolate then shift the R Type writeAddress
		currentInstruction.writeAddressIType = currentInstruction.read2Address;
		currentInstruction.shamt = (instruction & 1984) >> 6; //00000000000000000000011111000000 & Isolate then shift the shamt
		currentInstruction.funct = (instruction & 63); //00000000000000000000000000111111 & Isolate the funct
		currentInstruction.immediate = (instruction & 65535); //00000000000000001111111111111111 & Isolate the 16-bit immediate
		currentInstruction.immediate_26 = (instruction & 67108863); //00000011111111111111111111111111 & Isolate the last 26-bit immediate
		return currentInstruction;
	}

	uint32_t generateAluOp(uint32_t opCode, uint32_t funct)
	{
		switch (opCode)
		{
		case 0: //R-Type
			switch (funct)
			{
			case 32: //100000 - ADD
				return 2; //0010
				break;

			case 36: //100100 - AND
				return 0; //0000
				break;

			case 37: //100101 - OR
				return 1; //0001
				break;

			case 42: //101010 - SLT
				return 7; //0111
				break;

			case 0: //000000 - SLL
				return 6; //0110 NEED TO DOUBLE CHECK
				break;

			case 2: //000010 - SRL
				return 6; //0110 NEED TO DOUBLE CHECK
				break;

			case 34: //100010 - SUB
				return 6; //0110
				break;

			case 3: //000011 - SRA
				return 6; //0110 NEED TO DOUBLE CHECK
				break;

			case 38: //100110 - XOR
				return 6; //UNKNOWN FOR NOW NEED TO DOUBLE CHECK
				break;

			default:
				return 0;
				break;
			}
			break;


		case 8: //001000 - ADDI
			return 2; //0010
			break;

		case 12: //001100 - ANDI
			return 0; //0000
			break;

		case 4: //000100 - BEQ
			return 6; //0110 
			break;

		case 5: //000101 - BNE
			return 6; //0100 UNKNOWN FOR NOW NEED TO DOUBLE CHECK
			break;

		case 15: //001111 - LUI
			return 6; //0100 UNKNOWN FOR NOW NEED TO DOUBLE CHECK
			break;

		case 35: //100011 - LW
			return 2; //0010
			break;

		case 13: //001101 - ORI
			return 1; //0001 
			break;

		case 10: //001010 - SLTI
			return 7; //0111 
			break;

		case 43: //101011 - SW
			return 2; //0010
			break;

		case 14: //001110 - XORI
			return 6; //0110 NEED TO DOUBLE CHECK
			break;

		case 2: //000010 - J
			return 6; //0110 NEED TO DOUBLE CHECK
			break;

		case 3: //000011 - JAL
			return 6; //0110 NEED TO DOUBLE CHECK
			break;

		default:
			return 0;
			break;
		}
	}

	CUSignalType generateControlSignals(uint32_t opCode, uint32_t instruction)
	{
		CUSignalType signals;

		if (instruction == 0) //if no instruction is passed
		{
			signals.dePhase.Branch = 0;
			signals.dePhase.Jump = 0;
			signals.exPhase.ALUSrc = 0;
			signals.exPhase.RegDst = 0;
			signals.memPhase.MemRead = 0;
			signals.memPhase.MemWrite = 0;
			signals.writeBackPhase.MemtoReg = 0;
			signals.writeBackPhase.RegWrite = 0; //temporary
			return signals;
		}

		signals.exPhase.ALUSrc = 1;
		signals.exPhase.RegDst = 0;
		//signals.memPhase.Branch = 0;
		signals.memPhase.MemRead = 0;
		signals.memPhase.MemWrite = 0;
		signals.writeBackPhase.MemtoReg = 0;
		signals.writeBackPhase.RegWrite = 1; //temporary
		signals.dePhase.Branch = 0;
		signals.dePhase.Jump = 0;

		switch (opCode)
		{
		case 0: //R-Type
			signals.exPhase.ALUSrc = 0;
			signals.exPhase.RegDst = 1;
			break;

		case 43: //101011 - SW
			signals.memPhase.MemWrite = 1;
			signals.writeBackPhase.RegWrite = 0; //temporary
			break;

		case 35: //100011 - LW
			signals.writeBackPhase.MemtoReg = 1;
			signals.memPhase.MemRead = 1;
			signals.writeBackPhase.RegWrite = 1; //temporary
			break;

		case 4: //000100 - BEQ
		case 5: //000101 - BNE
			signals.dePhase.Branch = 1;
			signals.writeBackPhase.RegWrite = 0; //temporary
			break;

		case 2: //000010 - J
			signals.dePhase.Jump = 1;
			break;

		case 3: //000011 - JAL
			signals.dePhase.Jump = 1;
			break;
		}

		return signals;
	}

	ID_EX_buffer updateReadValues(InstructionComponents passedComponents, ID_EX_buffer outBuffer, uint32_t GPR[])
	{
		outBuffer.read1Cont = GPR[passedComponents.read1Address];

		outBuffer.read2Cont = GPR[passedComponents.read2Address];
		return outBuffer;
	}

	/***********************ALU FUNCTIONS*************************/


	uint32_t ALU(ID_EX_buffer *in, uint32_t Reg2)
	{
		/* ADD */
		if (in->aluOP == 2)
		{
			return (in->read1Cont + Reg2);
		}
		/* SUB */
		if (in->aluOP == 6)
		{
			return (in->read1Cont - Reg2);
		}
		/* AND */
		if (in->aluOP == 0)
		{
			return (in->read1Cont & Reg2);
		}
		/* OR */
		if (in->aluOP == 1)
		{
			return (in->read1Cont | Reg2);
		}
		/* Set on Less Than */
		if (in->aluOP == 7)
		{
			if (in->read1Cont < Reg2)
			{
				return 1;
			}
			return 0;
		}
		else
		{
			return 0;
		}
	}

	uint32_t Adder(ID_EX_buffer *in, uint32_t Reg2)
	{
		return (in->read1Cont + Reg2);
	}

	uint32_t Multiplexor(ID_EX_buffer *in)
	{
		if (in->signals.exPhase.ALUSrc == 1)
		{
			return in->extendedImmediate;
		}
		return in->read2Cont;
	}

	uint32_t PC_Counter(uint32_t PC)
	{
		return (PC + 4);
	}

	/*************************************************************/

	uint32_t rw_memory(uint32_t ALUresult, uint32_t data2, uint32_t MemWrite, uint32_t MemRead, uint32_t *memdata, uint32_t Mem[])
	{

		if ((MemWrite == 1 || MemRead == 1) && ALUresult % 4 != 0) // If address is bad, then return a halt condition
		{
			//Memory call out of range
			return 1;
		}
		//checks if MemWrite is 1. If it is, it sets memory of ALUresult to data2
		if (MemWrite == 1)
		{
			convertMemoryAddressToMemoryIndex(ALUresult);
			Mem[convertMemoryAddressToMemoryIndex(ALUresult)] = data2;
		}
		//checks if MemRead is 1. If it is, it sets the memory data to memory of ALUresult shifted 2-bits
		if (MemRead == 1)
		{
			*memdata = Mem[convertMemoryAddressToMemoryIndex(ALUresult)];
		}

		return 0;
	}

	//Write Register
	void write_register(uint32_t r2, uint32_t r3, uint32_t memdata, uint32_t ALUresult, uint32_t RegWrite, uint32_t RegDst, uint32_t MemtoReg, uint32_t *Reg)
	{
		if (RegWrite == 1)
		{
			if (MemtoReg == 1)
			{
				if (RegDst == 1)
					Reg[r3] = memdata; //Write memdata to rd
				else
					Reg[r2] = memdata; // Write memdata to rt
			}
			else
			{
				if (RegDst == 1)
					Reg[r3] = ALUresult; // Write ALU results to rd
				else
					Reg[r2] = ALUresult; // Write ALU results to rt
			}
		}
	}

	uint32_t changeToDecimal(char passedVal[], int valBase)
	{
		uint32_t returnValue;
		int generalCycler, multiplier;
		returnValue = 0;

		for (int generalCycler = 0; generalCycler < strlen(passedVal); generalCycler++)
		{
			multiplier = pow(16, (strlen(passedVal) - generalCycler - 1));
			returnValue += (modifyCharacter(passedVal[generalCycler]) * multiplier);
		}
		return returnValue;
	}

	int modifyCharacter(char passedCharacter)
	{
		if (passedCharacter > 57) //if greater than '9'
		{
			if (passedCharacter > FUVALUE) //if greater than 'F'
			{
				if (passedCharacter > FLVALUE) //if greater than 'f'
				{
					return -1; //error case
				}
				else
				{
					return (passedCharacter - ALVALUE + 10); //take away 'A' then add 10
				}
			}
			return (passedCharacter - AUVALUE + 10); //take away 'A' then add 10
		}
		else
		{
			return passedCharacter - 48;
		}
	}

	int convertPCToInstructionIndex(int givenPCValue)
	{
		int index = (givenPCValue - 536870912) / 4; //instruction starts at 0x20000000
		return index;
	}

	int convertMemoryAddressToMemoryIndex(uint32_t passedAddress)
	{
		int index = (passedAddress - 268435456) / 4; //mem starts at 0x10000000
		return index;
	}

	//Opcode 4 BRanch

	int BranchEqual(struct IF_ID_buffer* in, InstructionComponents Instruction, ID_EX_buffer* out)
	{
		if (cpu_ctx.GPR[Instruction.read1Address] == cpu_ctx.GPR[Instruction.read2Address])
		{
			in->next_pc += Instruction.immediate << 2; //Set PC by offset
			cpu_ctx.PC = in->next_pc - 4;
			setSignalsToZeroh(out);
			return 0;
		}

		else
		{
			return 0;
		}
	}

	int newBranchEqual(struct IF_ID_buffer* in, uint32_t value1, uint32_t value2, uint32_t immediate, ID_EX_buffer* out)
	{
		if (value1 == value2)
		{
			in->next_pc += immediate << 2; //Set PC by offset
			cpu_ctx.PC = in->next_pc - 4;
			setSignalsToZeroh(out);
			return 0;
		}
		return 0;
	}

	int BranchNotEqual(struct IF_ID_buffer* in, InstructionComponents Instruction, ID_EX_buffer* out)
	{
		if (cpu_ctx.GPR[Instruction.read1Address] != cpu_ctx.GPR[Instruction.read2Address])
		{
			in->next_pc += Instruction.immediate << 2;
			cpu_ctx.PC = in->next_pc - 4;
			setSignalsToZeroh(out);
			return 0;
		}

		else
		{
			return 0;
		}
	}

	int newBranchNotEqual(struct IF_ID_buffer* in, uint32_t value1, uint32_t value2, uint32_t immediate, ID_EX_buffer* out)
	{
		if (value1 != value2)
		{
			in->next_pc += immediate << 2; //Set PC by offset
			cpu_ctx.PC = in->next_pc - 4;
			setSignalsToZeroh(out);
			return 0;
		}
		return 0;
	}



	int jump(struct IF_ID_buffer *in, InstructionComponents Instructions, ID_EX_buffer* out)
	{
		uint32_t last28bits = Instructions.immediate_26 << 2; //Shifts immediate two to the left
		uint32_t first4bits = (in->next_pc & 4026531840); // PC and 11110000000000000000000000000000
		uint32_t newAddress = (last28bits | first4bits);
		in->next_pc = newAddress;
		setSignalsToZeroh(out);
		return 0;
	}

	int jumpAndLink(struct IF_ID_buffer *in, InstructionComponents Instructions, ID_EX_buffer* out)
	{
		cpu_ctx.GPR[31] = in->next_pc + 4;
		jump(in, Instructions, out);
		return 0;
	}

	int jumpRegister(struct IF_ID_buffer* in, ID_EX_buffer* out)
	{
		if (cpu_ctx.GPR[31] != 0)
		{
			in->next_pc = cpu_ctx.GPR[31];
			setSignalsToZeroh(out);
		}
		cpu_ctx.GPR[31] = 0;
		return 0;
	}

	int initializeSignals(struct IF_ID_buffer *if_id, ID_EX_buffer *id_ex, struct EX_MEM_buffer *ex_mem, struct MEM_WB_buffer *mem_wb)
	{
		(*id_ex).signals.dePhase.Branch = 0;
		(*id_ex).signals.dePhase.Jump = 0;
		(*id_ex).signals.exPhase.ALUSrc = 0;
		(*id_ex).signals.exPhase.RegDst = 0;
		(*id_ex).signals.memPhase.MemRead = 0;
		(*id_ex).signals.memPhase.MemWrite = 0;
		(*id_ex).signals.writeBackPhase.MemtoReg = 0;
		(*id_ex).signals.writeBackPhase.RegWrite = 0;

		(*ex_mem).signals = (*id_ex).signals;
		(*mem_wb).signals = (*id_ex).signals;

		squashed = squashed + 4; //starts by squashing last 4 phases

		return 0;
	}

	//Forward Instructions

	uint32_t forwarder(struct EX_MEM_buffer *EX_MEM, struct MEM_WB_buffer *MEM_WB, uint32_t incomingAddress, uint32_t exMemAddress, uint32_t wbAddress)
	{
		if (checkToForward(incomingAddress, exMemAddress, wbAddress) == 2)
		{
			return EX_MEM->address;
		}

		if (checkToForward(incomingAddress, exMemAddress, wbAddress) == 1)
		{
			return MultiplexorEX(MEM_WB);
		}
		return 0;
	}

	int checkToForward(uint32_t incomingAddress, uint32_t exMemAddress, uint32_t wbAddress)
	{
		if ((incomingAddress == exMemAddress) && (incomingAddress != 0))
		{
			return 2; // EX_MEM
		}

		if ((incomingAddress == wbAddress) && (incomingAddress != 0))
		{
			return 1; // MEM_WB
		}

		return 0;
	}

	uint32_t MultiplexorEX(struct MEM_WB_buffer* in)
	{
		if (in->signals.writeBackPhase.RegWrite == 1)
		{
			if (in->signals.writeBackPhase.MemtoReg == 1)
			{
				return in->memdata; // Write memdata 
			}
			else
			{
				return in->ALUresult; // Write ALU results to rd
			}
		}
	}

	int hazardDetection(struct IF_ID_buffer *in, InstructionComponents Instructions, ID_EX_buffer* out, hazard_data* hazardData)
	{
		if (Instructions.opCode == 35)
		{
			hazardData->loadIsTrue = 1;
			hazardData->address = Instructions.read2Address;
			return 0;
		}
		if (hazardData->loadIsTrue == 1)
		{
			//If load happened last instruction check if next instruction uses that address then stall else set back to 0
			if (hazardData->address == Instructions.read1Address || hazardData->address == Instructions.read2Address)
			{
				setSignalsToZeroh(out);
				cpu_ctx.PC -= 4;
				hazardData->address = 0;
				hazardData->loadIsTrue = 0;
				return 0;
			}
			else
			{
				hazardData->address = 0;
				hazardData->loadIsTrue = 0;
				return 0;
			}
		}
		else
		{
			//Reset these to 0 if no hazards transpire
			hazardData->address = 0;
			hazardData->loadIsTrue = 0;
			return 0;
		}
	}