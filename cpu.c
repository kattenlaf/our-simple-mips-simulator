/**
* Ruel Gordon
* Matthew King
* LeAnn Lewis
* Gedare Bloom
* cpu.c
*
* Implementation of simulated processor.
*/

#include "cpu.h"
#include "memory.h"

struct cpu_context cpu_ctx;

int fetch(struct IF_ID_buffer *out)
{
	(*out).instruction = instruction_memory[convertPCToInstructionIndex(cpu_ctx.PC)];
	(*out).next_pc = cpu_ctx.PC + 4;
	return 0;
}

//added exmem and memwb

int decode(struct IF_ID_buffer *in, ID_EX_buffer *out, struct EX_MEM_buffer *ex_mem, struct MEM_WB_buffer *mem_wb, hazard_data *hazardDetect)
{
	InstructionComponents currentInstruction;
	currentInstruction = separateComponents((*in).instruction);
	(*out).writeAddressIType = currentInstruction.writeAddressIType;
	(*out).writeAddressRType = currentInstruction.writeAddressRType;
	(*out).extendedImmediate = signExtendValue(currentInstruction.immediate);
	(*out).signals = generateControlSignals(currentInstruction.opCode, (*in).instruction);
	hazardDetection(in, currentInstruction, out, hazardDetect);
	//LeAnn's thing was here
	*out = updateReadValues(currentInstruction, *out, cpu_ctx.GPR);
	(*out).opCode = currentInstruction.opCode;
	(*out).funct = currentInstruction.funct;
	(*out).Cont1Address = currentInstruction.read1Address;
	(*out).Cont2Address = currentInstruction.read2Address;

	//Added branchforwarder

	//dest reg
	int destinationReg;
	if ((*out).signals.exPhase.RegDst == 1)
	{
		destinationReg = (*out).writeAddressRType;
	}
	else
	{
		destinationReg = (*out).writeAddressIType;
	}

	//Check To Forward

	int Check = checkToForward(out->Cont1Address, ex_mem->destinationReg, mem_wb->destinationReg);

	uint32_t branchval1 = out->read1Cont;
	uint32_t branchval2 = out->read2Cont;

	if (Check != 0)
	{
		branchval1 = forwarder(ex_mem, mem_wb, out->Cont1Address, ex_mem->destinationReg, mem_wb->destinationReg);
	}

	Check = checkToForward(out->Cont2Address, ex_mem->destinationReg, mem_wb->destinationReg); //Reassign Check

	if (Check != 0)
	{
		branchval2 = forwarder(ex_mem, mem_wb, out->Cont2Address, ex_mem->destinationReg, mem_wb->destinationReg);
	}

	if (currentInstruction.read1Address == 31 && currentInstruction.funct == 8) // jr $ra
	{
		jumpRegister(in, out);
	}

	if (out->signals.dePhase.Jump == 1) //J or Jal
	{
		if (currentInstruction.opCode == 2)
		{
			jump(in, currentInstruction, out);
		}
		else if (currentInstruction.opCode == 3)
		{
			jumpAndLink(in, currentInstruction, out);
		}
	}

	if (out->signals.dePhase.Branch == 1) //BEQ or BNE
	{
		if (currentInstruction.opCode == 4)
		{
			newBranchEqual(in, branchval1, branchval2, currentInstruction.immediate, out);
		}
		else if (currentInstruction.opCode == 5)
		{
			newBranchNotEqual(in, branchval1, branchval2, currentInstruction.immediate, out);
		}
	}

	if (currentInstruction.opCode == 0 && currentInstruction.funct == 12) //Syscall
	{
		syscall(out, cpu_ctx);
	}
	return 0;
}

int execute(ID_EX_buffer *in, struct EX_MEM_buffer *out, struct MEM_WB_buffer *MEM)
{
	uint32_t secondValue = 0;
	in->aluOP = generateAluOp(in->opCode, in->funct);
	//Responsible for handling what value should be passed to ALU


	//dest reg
	int destinationReg;
	if ((*in).signals.exPhase.RegDst == 1)
	{
		destinationReg = (*in).writeAddressRType;
	}
	else
	{
		destinationReg = (*in).writeAddressIType;
	}

	//Check To Forward

	int Check = checkToForward(in->Cont1Address, out->destinationReg, MEM->destinationReg);

	if (Check != 0)
	{
		in->read1Cont = forwarder(out, MEM, in->Cont1Address, out->destinationReg, MEM->destinationReg);
	}

	Check = checkToForward(in->Cont2Address, out->destinationReg, MEM->destinationReg); //Reassign Check

	if (Check != 0)
	{
		in->read2Cont = forwarder(out, MEM, in->Cont2Address, out->destinationReg, MEM->destinationReg);
	}

	(*out).writeAddressIType = (*in).writeAddressIType;
	(*out).writeAddressRType = (*in).writeAddressRType;
	secondValue = Multiplexor(in);
	out->address = ALU(in, secondValue);
	(*out).writeData = (*in).read2Cont;
	(*out).signals = (*in).signals;

	out->destinationReg = destinationReg;
	return 0;
}



int memory(struct EX_MEM_buffer* in, struct MEM_WB_buffer* out)
{
	out->ALUresult = in->address; //Address is stored in the MEM_
	(*out).memdata = (*in).address; // not sure if a fix
	rw_memory(in->address, in->writeData, in->signals.memPhase.MemWrite, in->signals.memPhase.MemRead, &out->memdata, data_memory);
	(*out).writeAddressIType = (*in).writeAddressIType;
	(*out).writeAddressRType = (*in).writeAddressRType;
	(*out).signals = (*in).signals;
	out->destinationReg = in->destinationReg;
	return 0;
}


int writeback(struct MEM_WB_buffer *in)
{
	write_register((*in).writeAddressIType, (*in).writeAddressRType, in->memdata, in->ALUresult, in->signals.writeBackPhase.RegWrite, in->signals.exPhase.RegDst, in->signals.writeBackPhase.MemtoReg, cpu_ctx.GPR);

	return 0;
}